Pod::Spec.new do |spec|

  spec.name         = "my-extensions"
  spec.version      = "0.0.1"
  spec.summary      = "A short description of framework."
  spec.description  = "A short description of framework description"
  spec.homepage     = "https://gitlab.com/inboxme.vamsee/my-extensions"
  spec.license      =  { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Vamsee" => "vamcios@gmail.com" }
  spec.platform     = :ios, "9.0"
  spec.source       = { :git => "git@gitlab.com:inboxme.vamsee/my-extensions.git", :tag => "#{spec.version}" }
  spec.source_files  = "my-extensions/my-extensions/**/*.{swift}"
  spec.exclude_files = "Classes/Exclude"

end
