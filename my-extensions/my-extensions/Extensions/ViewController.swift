//
//  ViewController.swift
//  FrameWork
//
//  Created by Vamsee Gudla on 20/08/20.
//  Copyright © 2020 Vamsee Gudla. All rights reserved.
//

import UIKit
import SDWebImage

public class FrameWorkManager {
    public var vc = UIViewController()
    
    public init() {}
    
    public func test() {
        print("Hello Buddy")
    }

    public func viewController() -> UIViewController {
        let storyboard = UIStoryboard(name: "Storyboard", bundle: Bundle.init(for: ViewController.self))
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController")
        return vc
    }
    
    public func seImage(_ image: String) {
        let vc = viewController() as? ViewController
        vc?.loadImage(image)
    }
    
}

public class ViewController: UIViewController {
    
    @IBOutlet public weak var imageView: UIImageView!
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    public func loadImage(_ input: String) {
                let imageURL = URL(string: input)!
//        imageView.sd_setShowActivityIndicatorView(true)
//        imageView.sd_setIndicatorStyle(.gray)
        imageView.sd_setImage(with: imageURL)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
